(;
 ; Advent of Code 2022: Day 2, in wasm
 ;   by Ashelyn Rose
 ;)

(module
  (memory (export "memory") 10)

  (global $readLoc (mut i32) (i32.const 0))
  (global $inputEnd (mut i32) (i32.const 0))
  (global $outputStart (mut i32) (i32.const 0))
  (global $outputEnd (mut i32) (i32.const 0))

  (func (export "part1") (result i32)
    (local $currentScore i32)
    (local $lastRead i32)
    call $initRead

    loop $readLoop
      call $readLine

      ;; check to see if we've hit the end
      local.tee $lastRead
      i32.eqz
      if
        call $initWrite

        local.get $currentScore
        call $writeNum
        
        call $closeWrite
        return
      end

      local.get $lastRead
      call $getScore
      local.get $currentScore
      i32.add
      local.set $currentScore
      br $readLoop
    end

    unreachable
  )

  (func (export "part2") (result i32)
    (local $currentScore i32)
    (local $lastRead i32)
    call $initRead

    loop $readLoop
      call $readLine

      ;; check to see if we've hit the end
      local.tee $lastRead
      i32.eqz
      if
        call $initWrite

        local.get $currentScore
        call $writeNum
        
        call $closeWrite
        return
      end

      local.get $lastRead
      call $solveRound
      call $getScore
      local.get $currentScore
      i32.add
      local.set $currentScore
      br $readLoop
    end

    unreachable
  )

  (func $initRead
    ;; Read input length
    i32.const 0
    i32.load

    ;; Store input end byte
    i32.const 4
    i32.add
    global.set $inputEnd

    ;; Set up start read location
    i32.const 4
    global.set $readLoc
  )

  (func $initWrite
    (local $dup i32)
    ;; set up output end pointer
    global.get $inputEnd
    i32.const 4
    i32.add
    global.set $outputStart
    global.get $outputStart
    global.set $outputEnd
  )

  (func $closeWrite (result i32)
    ;; get the length to return
    global.get $inputEnd
    global.get $outputEnd
    global.get $outputStart
    i32.sub
    i32.store

    ;; return location of length
    global.get $inputEnd
  )

  (func $solveRound (param $theirs i32) (param $outcome i32) (result i32 i32)
    (local $tmp i32)

    local.get $outcome
    i32.const 1
    i32.add

    local.get $theirs
    i32.add
    i32.const 3
    i32.rem_s
    local.tee $tmp

    i32.eqz
    if
      i32.const 3
      local.set $tmp
    end

    local.get $theirs
    local.get $tmp
  )

  (func $getScore (param $theirs i32) (param $ours i32) (result i32)
    (local $diff i32)

    ;; some modulo math to get ours - theirs (with wraparound)
    local.get $ours
    i32.const 3
    i32.add
    local.get $theirs
    i32.sub
    i32.const 3
    i32.rem_s
    local.set $diff

    block $checks
      local.get $diff
      i32.eqz;; if we tied
      if
        i32.const 3
        local.set $diff
        br $checks
      end

      ;; if we won
      local.get $diff
      i32.const 1
      i32.eq
      if
        i32.const 6
        local.set $diff
        br $checks
      end

      ;; if we lost
      local.get $diff
      i32.const 2
      i32.eq
      if
        i32.const 0
        local.set $diff
        br $checks
      end
    end

    local.get $diff
    local.get $ours
    i32.add
  )

  (func $readLine (result i32 i32)
    call $readChar
    call $readChar
  )


  (func $readChar (result i32)
    (local $charcode i32)
    ;; Make sure we're still inside the input area
    block $readLocCheck
      global.get $readLoc
      global.get $inputEnd
      i32.lt_u
      br_if $readLocCheck

      i32.const 0
      return
    end
    
    ;; Read the character
    global.get $readLoc
    i32.load8_u
    local.set $charcode


    ;; increment read location
    global.get $readLoc
    i32.const 1
    i32.add
    global.set $readLoc

    local.get $charcode
    call $translateChar
    local.tee $charcode

    ;; if we didn't find a valid char, keep reading until we do
    i32.eqz
    if
      call $readChar
      return
    end
    
    local.get $charcode
    return
  )

  (func $translateChar (param $input i32) (result i32)
    local.get $input
    i32.const 65 ;; A
    i32.eq
    if
      i32.const 1
      return
    end

    local.get $input
    i32.const 66 ;; B
    i32.eq
    if
      i32.const 2
      return
    end

    local.get $input
    i32.const 67 ;; C
    i32.eq
    if
      i32.const 3
      return
    end

    local.get $input
    i32.const 88 ;; X
    i32.eq
    if
      i32.const 1
      return
    end

    local.get $input
    i32.const 89 ;; Y
    i32.eq
    if
      i32.const 2
      return
    end

    local.get $input
    i32.const 90 ;; Z
    i32.eq
    if
      i32.const 3
      return
    end

    i32.const 0
    return
  )

  (func $writeNum (param $number i32)
    (local $digit i32)
    ;; store lowest digit
    local.get $number
    i32.const 10
    i32.rem_u
    local.set $digit

    ;; divide number by 10
    local.get $number
    i32.const 10
    i32.div_u
    local.tee $number

    ;; base case
    i32.eqz
    if
      local.get $digit
      call $writeDigit
      return
    end
    
    ;; recurse (to print other digits)
    local.get $number
    call $writeNum

    ;; print our digit
    local.get $digit
    call $writeDigit
  )

  (func $writeDigit (param $digit i32)
    global.get $outputEnd
    
    local.get $digit
    i32.const 48
    i32.add

    i32.store

    global.get $outputEnd
    i32.const 1
    i32.add
    global.set $outputEnd
  )
)
