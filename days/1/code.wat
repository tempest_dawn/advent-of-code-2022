(;
 ; Advent of Code 2022: Day 1, in wasm
 ;   by Ashelyn Rose
 ;)

(module
  (memory (export "memory") 10)

  (global $readLoc (mut i32) (i32.const 0))
  (global $inputEnd (mut i32) (i32.const 0))
  (global $outputEnd (mut i32) (i32.const 0))

  (func (export "part1") (result i32)
    (local $currentMaxCals i32)
    (local $currentElf i32)
    ;; Read input length
    i32.const 0
    i32.load

    ;; Store input end byte
    i32.const 4
    i32.add
    global.set $inputEnd

    ;; Set up start read location
    i32.const 4 ;; first memory address after length u32
    global.set $readLoc

    i32.const 0
    local.set $currentMaxCals

    loop $readLoop
      local.get $currentMaxCals
      call $readElf
      local.tee $currentElf
      i32.lt_u
      if
        local.get $currentElf
        local.set $currentMaxCals
      end

      global.get $readLoc
      global.get $inputEnd
      i32.lt_s
      br_if $readLoop
    end $readLoop

    ;; set up output end pointer
    global.get $inputEnd
    i32.const 4
    i32.add
    global.set $outputEnd

    ;; write the number
    local.get $currentMaxCals
    call $writeNum

    ;; get the length to return
    global.get $inputEnd
    global.get $outputEnd
    global.get $inputEnd
    i32.const 4
    i32.add
    i32.sub
    i32.store

    ;; return location of length
    global.get $inputEnd
  )

  (func (export "part2") (result i32)
    (local $currentElf i32)
    (local $maxCals1 i32)
    (local $maxCals2 i32)
    (local $maxCals3 i32)
    ;; Read input length
    i32.const 0
    i32.load

    ;; Store input end byte
    i32.const 4
    i32.add
    global.set $inputEnd

    ;; Set up start read location
    i32.const 4 ;; first memory address after length u32
    global.set $readLoc

    i32.const 0
    local.set $maxCals1
    i32.const 0
    local.set $maxCals2
    i32.const 0
    local.set $maxCals3

    loop $readLoop
      call $readElf
      local.set $currentElf

      block $comparisons

        ;; compare with stored value 1 (highest)
        local.get $maxCals1
        local.get $currentElf
        i32.lt_u
        if
          ;; pick up new max 3 (in order)
          local.get $maxCals2
          local.get $maxCals1
          local.get $currentElf

          local.set $maxCals1
          local.set $maxCals2
          local.set $maxCals3
          br $comparisons
        end

        ;; compare with stored value 2
        local.get $maxCals2
        local.get $currentElf
        i32.lt_u
        if
          ;; pick up new second and third max (in order)
          local.get $maxCals2
          local.get $currentElf

          local.set $maxCals2
          local.set $maxCals3
          br $comparisons
        end

        ;; compare with stored value 3
        local.get $maxCals3
        local.get $currentElf
        i32.lt_u
        if
          local.get $currentElf
          local.set $maxCals3
          br $comparisons
        end

      end ;; $comparisons

      global.get $readLoc
      global.get $inputEnd
      i32.lt_s
      br_if $readLoop
    end ;; $readLoop

    ;; add up top 3
    local.get $maxCals1
    local.get $maxCals2
    local.get $maxCals3
    i32.add
    i32.add
    local.set $maxCals1


    ;; set up output end pointer
    global.get $inputEnd
    i32.const 4
    i32.add
    global.set $outputEnd

    ;; write the number
    local.get $maxCals1
    call $writeNum

    ;; get the length to return
    global.get $inputEnd
    global.get $outputEnd
    global.get $inputEnd
    i32.const 4
    i32.add
    i32.sub
    i32.store

    ;; return location of length
    global.get $inputEnd
  )

  (func $readElf (result i32)
    (local $currentSum i32)
    (local $mostRecent i32)

    loop $readLoop
      ;; read next number in file
      call $readNumber
      local.tee $mostRecent

      ;; if the number was 0
      i32.eqz
      if
        ;; return sum
        local.get $currentSum
        return
      end

      ;; else add number to sum and continue
      local.get $currentSum
      local.get $mostRecent
      i32.add
      local.set $currentSum
      br $readLoop
    end

    i32.const -1
  )

  (func $readNumber (result i32)
    (local $number i32)
    (local $currentDigit i32)
    i32.const 0
    local.set $number

    loop $readLoop
      call $readDigit
      local.tee $currentDigit

      ;; check if less than 0
      i32.const 0
      i32.lt_s
      if
        local.get $number
        return
      end

      ;; shift number left
      local.get $number
      i32.const 10
      i32.mul

      ;; add digit
      local.get $currentDigit
      i32.add
      local.set $number
      br $readLoop
    end

    i32.const 0
  )

  (func $readDigit (result i32)
    (local $charcode i32)
    global.get $readLoc
    i32.load8_u
    local.tee $charcode

    ;; increment read location
    global.get $readLoc
    i32.const 1
    i32.add
    global.set $readLoc

    ;; check if out of range (lower)
    i32.const 48
    i32.lt_u
    if
      i32.const -1
      return
    end

    ;; check if out of range (upper)
    local.get $charcode
    i32.const 57
    i32.gt_u
    if
      i32.const -1
      return
    end

    local.get $charcode
    i32.const 48
    i32.sub
  )

  (func $writeNum (param $number i32)
    (local $digit i32)
    ;; store lowest digit
    local.get $number
    i32.const 10
    i32.rem_u
    local.set $digit

    ;; divide number by 10
    local.get $number
    i32.const 10
    i32.div_u
    local.tee $number

    ;; base case
    i32.eqz
    if
      local.get $digit
      call $writeDigit
      return
    end
    
    ;; recurse (to print other digits)
    local.get $number
    call $writeNum

    ;; print our digit
    local.get $digit
    call $writeDigit
  )

  (func $writeDigit (param $digit i32)
    global.get $outputEnd
    
    local.get $digit
    i32.const 48
    i32.add

    i32.store

    global.get $outputEnd
    i32.const 1
    i32.add
    global.set $outputEnd
  )
)
